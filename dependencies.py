import platform
from shutil import which
from urllib.request import urlopen

plat = platform.system()


def print_open(url: str):
    print(url)
    urlopen(url)


if plat == 'Windows':
    if which('winget') is None:
        print('winget not found, please install it')
        print_open(f'https://learn.microsoft.com/fr-fr/windows/package-manager/winget/#install-winget')
        print(f'Then add `%UserProfile%\\AppData\\Local\\Microsoft\\WindowsApps` to path:')
        print('For that, use `sysdm.cpl ,3`')
        input('')
        exit()
    if which('ffmpeg') is None:
        print('ffmpeg not found, please install it through winget:')
        print('winget install ffmpeg')
    if which('python') is None:
        print('python not found, please install it through winget')
        print('winget install -e --id Python.Python.3.10')
        input('')
        exit()
elif plat == 'Linux':
    if which('ffmpeg') is None:
        print('ffmpeg not found, please install it')
    if which('python') is None:
        print('python not found, please install it')
        input('')
        exit()
elif plat == 'Darwin':
    if which('brew') is None:
        print('brew not found, please install it')
        print_open('https://brew.sh/')
        input('')
        exit()
    if which('ffmpeg') is None:
        print('ffmpeg not found, please install it through brew:')
        print('brew install ffmpeg')
    if which('python') is None:
        print('python not found, please install it')
        print('brew install python@3.10')
        input('')
        exit()

print('All dependencies are installed !')
input('')
