from pathlib import Path
import subprocess


def get_video_duration(video_path):
    result = subprocess.run(
        ["ffprobe", "-v", "error", "-show_entries", "format=duration", "-of",
         "default=noprint_wrappers=1:nokey=1", video_path],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )
    return float(result.stdout)


def trim_video(video_path, offset, duration, output_path):
    offset /= 1000
    duration /= 1000

    subprocess.run(
        ["ffmpeg", "-i", video_path, "-ss", str(offset), "-t", str(duration), output_path],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )


ee = sorted(Path(".").glob("cam*_1.mp4"))

ff = list(map(lambda x: int(get_video_duration(str(x)) * 1000), ee))
print(ff)

ll = []

for i, el in enumerate(ff):
    length = input(f'Frame de référence de {ee[i]}:  ')
    try:
        length = int(length)
        if length > el:
            raise ValueError("ee")
    except ValueError:
        print('Entrez un chiffre')
        exit()
    ll.append(length)

ff = list(map(lambda x: [x[0], x[1]], zip(ll, ff)))

print(ff)

ref = min(map(lambda x: x[0], ff))
after = min(map(lambda x: x[1] - x[0], ff))
pairs = list(map(lambda x: (x[0] - ref, x[0] - (x[0] - ref) + after), ff))
# pairs = list(map(lambda x: (x[0] - ref, x[1] + ref - x[0]), ff))
# pairs = list(map(lambda x: (ref - x[0], ref + after), ff))
print(pairs)

for i, j in zip(ee, pairs):
    print(str(i))
    print(int(get_video_duration(str(i)) * 1000))
    print(trim_video(str(i), j[0], j[1], str(i)[:-4] + '_out.mp4'))

ee = sorted(Path(".").glob("cam*_*.mp4"))

for i in ee:
    print(str(i))
    print(int(get_video_duration(str(i)) * 1000))
input('')
