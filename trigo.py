import math

b = int(input("hauteur: ")) / 2
a = int(input("largeur: ")) / 2
ratio = int(input("ratio1: "))
ratio /= int(input("ratio2: "))
alpha = int(input("angle: ")) / 2
alpha2 = alpha / ratio
alpha = math.radians(alpha)
alpha2 = math.radians(alpha2)
b2 = b / math.tan(alpha)
b3 = a / math.tan(alpha2)
a = max(b2, b3)
print(f'Minimum for both constraints: {a:.2f}')
a = min(b2, b3)
b *= ratio * 2
print(f'Framed height for both constraints: {b:.2f}')
print(f'Minimum for minimal constraints: {a:.2f}')
input()
